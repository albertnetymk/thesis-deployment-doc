# heroku auto deploy

> NOTE: Travis-ci has builtin support for this.

Heroku is one quite popular Platform as a Server (Paas), supporting most
popular frameworks, such as Ruby on Rails, Node.js, Django, etc. Developers
could deploy their apps using git, which most developers use for version
control. Therefore, the integration is quite smooth. Meanwhile, as for open
source projects, developers usually put the source in one code hosting
provider, such as bitbucket or github. Then, is it possible to auto pull the
latest code from one code hosting provider and deploy without developers'
intervention.

Therefore, we would like to build one web service to provide this monitoring
functionality, so that developers could just push source code to the code
hosting provider. Then the code hosting provider notifies the web service.
Finally, the web service pulls the latest source code, and deploys it.
